package com.example.bruno.eduqode;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity{

    public static final int REQUEST_CODE = 0;
    private TextView result;
    private ImageButton scanBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        result = (TextView) findViewById(R.id.result);
        scanBtn = (ImageButton) findViewById(R.id.scanBtn);

    }

    public void callZXing(View view){
        Intent intent = new Intent(MainActivity.this, com.google.zxing.client.android.CaptureActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(REQUEST_CODE == requestCode && RESULT_OK == resultCode){
            result.setTextSize(16);
            result.setTextColor(Color.rgb(255, 255, 255));
            result.setText(data.getStringExtra("SCAN_RESULT") + " (" + data.getStringExtra("SCAN_FORMAT")+")");
        }
    }



}
